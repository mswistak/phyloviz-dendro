package net.phyloviz.dendro;

import net.phyloviz.dendro.ui.SaveDendroAction;
import org.openide.util.actions.NodeAction;
import org.openide.util.lookup.ServiceProvider;


@ServiceProvider(service = SaverProvider.class)
public class DendroSaverProvider extends SaverProvider {
    
    private static final String customName = "Dendro";
    private NodeAction action;

    @Override
    public String toString() {
            return customName;
    }

    public boolean asynchronous() {
            return false;
    }

    @Override
    public NodeAction getAction() {
            if (action == null)
                    action = new SaveDendroAction();
            return action;
    }

}