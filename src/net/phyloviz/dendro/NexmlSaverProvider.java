package net.phyloviz.dendro;

import net.phyloviz.dendro.ui.SaveNexmlAction;
import org.openide.util.actions.NodeAction;
import org.openide.util.lookup.ServiceProvider;


@ServiceProvider(service = SaverProvider.class)
public class NexmlSaverProvider extends SaverProvider {
    
    private static final String customName = "Nexml";
    private NodeAction action;

    @Override
    public String toString() {
            return customName;
    }

    public boolean asynchronous() {
            return false;
    }

    @Override
    public NodeAction getAction() {
            if (action == null)
                    action = new SaveNexmlAction();
            return action;
    }

}