package net.phyloviz.dendro.ui;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import net.phyloviz.dendro.SaverProvider;
import org.openide.util.Lookup;
import org.openide.util.actions.Presenter;


public class SaverMenuAction extends AbstractAction implements Presenter.Popup {

    @Override
    public void actionPerformed(ActionEvent e) {
        // No action.
    }

    @Override
    public JMenuItem getPopupPresenter() {
        JMenu result = new JMenu("Save tree");

        Collection<? extends SaverProvider> lookres = Lookup.getDefault().lookupAll(SaverProvider.class);
        Iterator<? extends SaverProvider> ir = lookres.iterator();
        while (ir.hasNext()) {
            JMenuItem mi = new JMenuItem();
            SaverProvider ap = ir.next();
            mi.setText(ap.toString());
            mi.addActionListener(ap.getAction());
            result.add(mi);
        }

        if (lookres.isEmpty())
            result.setEnabled(false);

        return result;
    }

}
