package net.phyloviz.dendro.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;
import net.phyloviz.algo.AbstractDistance;
import net.phyloviz.algo.Edge;
import net.phyloviz.alseq.AlignedSequence;
import net.phyloviz.alseq.AlignedSequenceFasta;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TypingData;
import net.phyloviz.dendro.SaveAction;
import net.phyloviz.dendro.tree.Tree;
import net.phyloviz.goeburst.tree.GOeBurstNode;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;


public class SaveNewickAction extends SaveAction {

    @Override
    protected TreeSaver createTreeSaver(TypingData<? extends Profile> td, AbstractDistance<GOeBurstNode> ad, Collection<Edge<GOeBurstNode>> edges) {
    /*
     * Creates TreeSaver object for saving Newick trees.
     */
        Profile p = td.iterator().next();
        TreeSaver ts = null;
        
        //Check the type of Profile in Typing Data.
        //Different summaries are available for different Profile types.
        if(p instanceof AlignedSequence || p instanceof AlignedSequenceFasta){
            
            //Display dialog asking if SMUT summary is to be computed.
            String SMUTmessage = "Do you want to save SMUT summary?\n\n" +
                    "The result is fully reliable only in case the file contains no indels. " +
                    "It is the responsibility of the user to give a properly aligned file as input.";
            Object userChose = DialogDisplayer.getDefault().notify(
                    new NotifyDescriptor.Confirmation(SMUTmessage, NotifyDescriptor.YES_NO_CANCEL_OPTION));
            
            //Act according to user's wish.
            //Construct appropriate tree saver objects.
            if (NotifyDescriptor.OK_OPTION.equals(userChose)) {
                ts = new NewickTreeSaver(new Tree(edges, ad, td), true);
            } else if(NotifyDescriptor.NO_OPTION.equals(userChose)){
                ts = new NewickTreeSaver(new Tree(edges, ad, td), false);
            } else if(NotifyDescriptor.CANCEL_OPTION.equals(userChose)){
                ;
            }
        } else {
            
            //Additional summary is not supported for this Profile type.
            //Construct appropriate tree saver object.
            ts = new NewickTreeSaver(new Tree(edges, ad, td), false);
        }
        return ts;
    }
    
    protected class NewickTreeSaver extends TreeSaver {
        
        public NewickTreeSaver(Tree tree, boolean smut){
            super(tree, smut, ".tree");
        }

        @Override
        protected void saveTree(File f, Tree tree) throws FileNotFoundException  {
        /*
         * Saves the tree structure in Newick format.
         */
            PrintWriter pw = new PrintWriter(f);
            pw.print(tree.Newick());
            pw.close();
        }
    }
}
