package net.phyloviz.dendro;

import org.openide.util.actions.NodeAction;


public abstract class SaverProvider {

	public abstract NodeAction getAction();
}