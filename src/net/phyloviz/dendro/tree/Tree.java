package net.phyloviz.dendro.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import javax.swing.table.AbstractTableModel;
import net.phyloviz.algo.AbstractDistance;
import net.phyloviz.algo.Edge;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TypingData;
import net.phyloviz.goeburst.tree.GOeBurstNode;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;


public class Tree {

    private TreeNode root;
    private TypingData<? extends Profile> td;
    private ArrayList<ArrayList<Integer>> smut = null;
    private AbstractDistance<GOeBurstNode> ad;

    private enum Field {
    /*
     * Simplifies calling specific columns when creating SMUT summary.
     */
        DIFFS(0), AC(1), AT(2), AG(3), CA(4), CT(5), CG(6), TA(7), TC(8), TG(9),
        GA(10), GC(11), GT(12), SUM(13), TRANSITION(14), TRANSVERSION(15),
        SILENT(16), NON_SILENT(17), EQUIVALENT(18), NON_EQUIV(19);
        private int idx;

        private Field(int idx){
            this.idx = idx;
        }

        public int getIdx(){
            return idx;
        }
    }

    private enum Codon{
    /*
     * Implements standard genetic code.
     * Keeps also track of aminoacids' properties.
     */
        AAA("K", 2), AAC("N", 0), AAG("K", 2), AAT("N", 0),
        ACA("T", 0), ACC("T", 0), ACG("T", 0), ACT("T", 0),
        AGA("R", 2), AGC("S", 0), AGG("R", 2), AGT("S", 0),
        ATA("I"), ATC("I"), ATG("M"), ATT("I"),

        CAA("Q", 0), CAC("H", 2), CAG("Q", 0), CAT("H", 2),
        CCA("P"), CCC("P"), CCG("P"), CCT("P"),
        CGA("R", 2), CGC("R", 2), CGG("R", 2), CGT("R", 2),
        CTA("L"), CTC("L"), CTG("L"), CTT("L"),

        GAA("E", 1), GAC("D", 1), GAG("E", 1), GAT("D", 1),
        GCA("A"), GCC("A"), GCG("A"), GCT("A"),
        GGA("G"), GGC("G"), GGG("G"), GGT("G"),
        GTA("V"), GTC("V"), GTG("V"), GTT("V"),

        TAA(), TAC("Y", 0), TAG(), TAT("Y", 0),
        TCA("S", 0), TCC("S", 0), TCG("S", 0), TCT("S", 0),
        TGA(), TGC("C", 0), TGG("W"), TGT("C", 0),
        TTA("L"), TTC("F"), TTG("L"), TTT("F");

        private String AA;
        private Boolean STOP;
        private Boolean acidic;
        private Boolean basic;
        private Boolean hydrophilic;

        private Codon(){
            this.AA = "STOP";
            this.STOP = true;
            this.acidic = false;
            this.basic = false;
            this.hydrophilic = false;
        }

        private Codon(String AA){
            this.AA = AA;
            this.STOP = false;
            this.acidic = false;
            this.basic = false;
            this.hydrophilic = false;
        }

        private Codon(String AA, int property){
            this.AA = AA;
            this.STOP = false;
            this.acidic = false;
            this.basic = false;
            this.hydrophilic = true;
            switch(property){
                case 1:
                    this.acidic = true;
                    break;
                case 2:
                    this.basic = true;
            }
        }

        public String getAA(){
            return AA;
        }

        public boolean isAcidic(){
            return acidic;
        }

        public boolean isBasic(){
            return basic;
        }

        public boolean isHydrophilic(){
            return hydrophilic;
        }

        public boolean isSTOP(){
            return STOP;
        }

        public boolean isSilent(Codon c){
            return AA.equals(c.getAA());
        }

        public boolean isEquivalent(Codon c){
            return acidic.equals(c.isAcidic()) && basic.equals(c.isBasic()) && hydrophilic.equals(c.isHydrophilic()) && STOP.equals(c.isSTOP());
        }
    }

    public Tree(Collection<Edge<GOeBurstNode>> edges, AbstractDistance<GOeBurstNode> ad, TypingData<? extends Profile> td) {
        this.ad = ad;
        this.td = td;

        //Create map of connections.
        Map<GOeBurstNode, ? extends Set<Edge<GOeBurstNode>>> connectionsMapping = mapConnections(edges);

        //Get set of nodes.
        Set<GOeBurstNode> keys = connectionsMapping.keySet();

        //Get one node to become the root.
        Iterator<GOeBurstNode> ni = keys.iterator();
        GOeBurstNode r = ni.next();

        //Wrap the node in the TreeNode class.
        //Recursively grow the Tree.
        root = new TreeNode(r, connectionsMapping, ad);//this.ad);

        //Set the root that minimizes the Tree's height.
        improveTree();
    }

    public StringBuilder Newick() {
    /*
     * Invoke constructing Newick-formated tree output.
     */
        return (root == null) ? new StringBuilder("") : root.Newick();
    }

    public StringBuilder Dendro() {
    /*
     * Invoke constructing Dendro-formated tree output.
     */
        StringBuilder out = new StringBuilder("#DENDROSCOPE\n{TREE 'T1'\n");
        out.append(Newick());
        out.append("\n}");
        return out;
    }

    public StringBuilder Nexml() {
    /*
     * Invoke constructing Nexml-formated tree output.
     */
        
        //Begin building output.
        StringBuilder out = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        out.append("<nex:nexml version=\"0.9\" " + 
                   "xsi:schemaLocation=\"http://www.nexml.org/2009 http://www.nexml.org/2009/nexml.xsd\" " +
                   "generator=\"PHYLOViZ Dendro 1.0\">\n");
        out.append("\t<otus id=\"os1\" />\n");
        out.append("\t<trees otus=\"os1\" id=\"ts1\">\n");
        
        
        if(root != null){
            out.append("\t\t<tree xsi:type=\"nex:FloatTree\" id=\"te1\">\n");
            
            //Initialize required variables and structures.
            TreeNode current;
            Deque<TreeNode> queue = new LinkedList<TreeNode>();
            Map<String, Integer> nodeMap = new HashMap<String, Integer>();
            
            StringBuilder edges = new StringBuilder("");
            StringBuilder nodes = new StringBuilder("");
            
            int edgeCount = 0;
            int nodeCount = 0;
            
            //Start with the root.
            queue.add(root);

            while( !queue.isEmpty() ){
                //Get first from the queue and add its children.
                current = queue.pollFirst();
                queue.addAll(current.getChildren());
                
                //Assign id for current node.
                nodeMap.put(current.getSelf().getID(), nodeCount);
                
                //Add new node to the "list" of nodes.
                nodes.append("\t\t\t<node label=\"" +
                             current.getSelf().getID() +
                             "\" id=\"node" + 
                             nodeMap.get(current.getSelf().getID()) +
                             "\" />\n");
                
                if(current.getParent() != null)
                    //Add new edge to the "list" of edges.
                    edges.append("\t\t\t<edge source=\"node" +
                                 nodeMap.get(current.getParent().getID()) +
                                 "\" length=\"" +
                                 ad.level(current.getParent(), current.getSelf()) +
                                 "\" target=\"node" +
                                 nodeMap.get(current.getSelf().getID()) +
                                 "\" id=\"edge" +
                                 edgeCount + 
                                 "\" />\n");
                
                edgeCount++;
                nodeCount++;
            }
            
            //Add nodes and edges to the output.
            out.append(nodes);
            out.append("\n");
            out.append(edges);
            out.append("\t\t</tree>\n");
        }
        
        //Finalize output.
        out.append("\t</trees>\n");
        out.append("</nex:nexml>");
        
        return out;
    }

    public StringBuilder[] MUT(boolean smut_flag) {
    /*
     * Generating MUT and SMUT summaries.
     */

        //Initiate builder to header of MUT summary.
        StringBuilder mut_sb = new StringBuilder("PARENT,CHILD,POSITION,MUTATION\n");

        //Start with the root and its children.
        TreeNode current = root;
        Deque<TreeNode> queue = new LinkedList<TreeNode>();
        queue.addAll(current.getChildren());

        //Is SMUT summary to be generated?
        if(smut_flag){
            //Initiate an (size)x20 array with zeros for storing SMUT data.
            //SMUT summary has 20 columns.
            //For names of the columns see: private enum Field.
            int size = current.getSelf().profileLength();
            smut = new ArrayList<ArrayList<Integer>>(size);

            for (int i = 0; i < size; i++) {
                ArrayList<Integer> row = new ArrayList<Integer>(20);

                for (int j = 0; j < 20; j++)
                    row.add(j, 0);

                smut.add(i, row);
            }
        }

        while( !queue.isEmpty() ){
            //Get first from the queue and add its children.
            current = queue.pollFirst();
            queue.addAll(current.getChildren());

            try{
                //Calculate mutations between current and its parent.
                //If smut_flag == true, also update SMUT array.
                mut_sb.append(MUT(current, smut_flag));
            } catch(IllegalArgumentException iae){
                //Non-standard nucleotide encountered.
                //SMUT summary won't be generated anymore due to non-standard input.
                //Reset SMUT array.
                smut = null;
                smut_flag = false;

                //There is need to rerun the expression from try clause.
                mut_sb.append(MUT(current, smut_flag));
            }
        }

        //Prepare output.
        StringBuilder[] out = new StringBuilder[2];
        out[0] = mut_sb;
        out[1] = SMUT();
        return out;
    }

    private HashMap<GOeBurstNode, HashSet<Edge<GOeBurstNode>>> mapConnections(Collection<Edge<GOeBurstNode>> edges) {
    /*
     * Create a map of connections between Nodes (observations).
     * For each Node we keep a set of Edges with one end attached. 
     */
        HashMap<GOeBurstNode, HashSet<Edge<GOeBurstNode>>> map = new HashMap<GOeBurstNode, HashSet<Edge<GOeBurstNode>>>();

        GOeBurstNode  u;
        GOeBurstNode v;
        Edge<GOeBurstNode> e;

        //Iterate over Edges in input collection.
        Iterator<Edge<GOeBurstNode>> ei = edges.iterator();
        
	while (ei.hasNext()) {
            
            //Get Edge and extract both its ends.
            e = ei.next();
            u = e.getU();
            v = e.getV();

            //Add Edge to map.
            map = addMapping(map, u, e);
            map = addMapping(map, v, e);
        }

        return map;
    }

    private HashMap<GOeBurstNode, HashSet<Edge<GOeBurstNode>>> addMapping(HashMap<GOeBurstNode, HashSet<Edge<GOeBurstNode>>> map, GOeBurstNode n, Edge<GOeBurstNode> e) {
    /*
     * Add Edge to map.
     */
        HashSet<Edge<GOeBurstNode>> set;

        //Check whether Node already is in the map.
        if(!map.containsKey(n)){
            set = new HashSet<Edge<GOeBurstNode>>();
        }else{
            set = map.get(n);
        }

        //Update map.
        set.add(e);
        map.put(n, set);

        return map;
    }

    private void improveTree() {
    /*
     * Set root so as to get the best possible one.
     */
        TreeNode tmp = root;
        root = root.improve();

        //Repeat until there is no improvement.
        while(!root.equals(tmp)){
            tmp = root;
            root = root.improve();
        }
    }
    
    private StringBuilder MUT(TreeNode current, boolean smut_flag) throws IllegalArgumentException {
    /*
     * Build MUT summary fragment for current TreeNode.
     * List all mutations between current and its parent.
     */
        //Prepare stream.
        StringBuilder mut_sb = new StringBuilder("");

        //Get profiles of current and its parent.
        Profile pprof = current.getParent().getProfile();
        Profile sprof = current.getSelf().getProfile();

        //Compare values one by one.
        for (int i = 0; i < pprof.profileLength(); i++) {
            
            //Encountering difference save it.
            if ( !pprof.getValue(i).equals(sprof.getValue(i)) ) {
                StringBuilder tmp = new StringBuilder();
                mut_sb.append(
                    tmp.append(current.getParent().getID()).append(",").
                        append(current.getSelf().getID()).append(",").
                        append(i+1).append(",").
                        append(pprof.getValue(i)).append("=>").
                        append(sprof.getValue(i)).append("\n")
                );

                //Update SMUT summary if required.
                if (smut_flag){
                    addMutation(pprof, sprof, i);
                }
            }
        }
        return mut_sb;
    }

    private void addMutation(Profile pprof, Profile sprof, int i) throws IllegalArgumentException {
    /*
     * Update SMUT summary.
     * Increment mutation count on row (profile position) i.
     */
        try{
            //Get appropriate row from SMUT.
            ArrayList<Integer> row = smut.get(i);
            
            if(!pprof.getValue(i).equals("-") && !sprof.getValue(i).equals("-")){

                //Check substitution type.
                //Increment appropriate columns.
                switch( Field.valueOf((pprof.getValue(i) + sprof.getValue(i)).toUpperCase()) ) {
                    case AC:    //adenine to cytosine
                        row.set(Field.AC.getIdx(), 1+row.get(Field.AC.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                    case AG:    //adenine to guanine
                        row.set(Field.AG.getIdx(), 1+row.get(Field.AG.getIdx()));
                        row.set(Field.TRANSITION.getIdx(), 1+row.get(Field.TRANSITION.getIdx()));
                        break;
                    case AT:    //adenine to thymine
                        row.set(Field.AT.getIdx(), 1+row.get(Field.AT.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                    case CA:    //cytosine to adenine
                        row.set(Field.CA.getIdx(), 1+row.get(Field.CA.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                    case CG:    //cytosine to guanine
                        row.set(Field.CG.getIdx(), 1+row.get(Field.CG.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                    case CT:    //cytosine to thymine
                        row.set(Field.CT.getIdx(), 1+row.get(Field.CT.getIdx()));
                        row.set(Field.TRANSITION.getIdx(), 1+row.get(Field.TRANSITION.getIdx()));
                        break;
                    case GA:    //guanine to adenine
                        row.set(Field.GA.getIdx(), 1+row.get(Field.GA.getIdx()));
                        row.set(Field.TRANSITION.getIdx(), 1+row.get(Field.TRANSITION.getIdx()));
                        break;
                    case GC:    //guanine to cytosine
                        row.set(Field.GC.getIdx(), 1+row.get(Field.GC.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                    case GT:    //guanine to thymine
                        row.set(Field.GT.getIdx(), 1+row.get(Field.GT.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                    case TA:    //thymine to adenine
                        row.set(Field.TA.getIdx(), 1+row.get(Field.TA.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                    case TC:    //thymine to cytosine
                        row.set(Field.TC.getIdx(), 1+row.get(Field.TC.getIdx()));
                        row.set(Field.TRANSITION.getIdx(), 1+row.get(Field.TRANSITION.getIdx()));
                        break;
                    case TG:    //thymine to guanine
                        row.set(Field.TG.getIdx(), 1+row.get(Field.TG.getIdx()));
                        row.set(Field.TRANSVERSION.getIdx(), 1+row.get(Field.TRANSVERSION.getIdx()));
                        break;
                }

                //Check positon in codon.
                int p1, p2, p3;
                if ((i % 3) == 0) { //first position in codon
                    p1 = 0;
                    p2 = 1;
                    p3 = 2;
                }else if ((i % 3) == 1) { //second position in codon
                    p1 = -1;
                    p2 = 0;
                    p3 = 1;
                }else { //third position in codon
                    p1 = -2;
                    p2 = -1;
                    p3 = 0;
                }

                //Extract appropriate codons from parental and current sequences.
                Codon c1 = Codon.valueOf(pprof.getValue(i+p1) + pprof.getValue(i+p2)+ pprof.getValue(i+p3));
                Codon c2 = Codon.valueOf(sprof.getValue(i+p1) + sprof.getValue(i+p2)+ sprof.getValue(i+p3));

                //Check type of introduced mutation in aminoacid sequence.
                if (c1.isSilent(c2)) {
                    row.set(Field.SILENT.getIdx(), 1+row.get(Field.SILENT.getIdx()));
                    row.set(Field.EQUIVALENT.getIdx(), 1+row.get(Field.EQUIVALENT.getIdx()));
                }else {
                    row.set(Field.NON_SILENT.getIdx(), 1+row.get(Field.NON_SILENT.getIdx()));
                    if(c1.isEquivalent(c2)) {
                        row.set(Field.EQUIVALENT.getIdx(), 1+row.get(Field.EQUIVALENT.getIdx()));
                    }else {
                        row.set(Field.NON_EQUIV.getIdx(), 1+row.get(Field.NON_EQUIV.getIdx()));
                    }
                }
            }
            
            //Increment summaric count at that position.
            row.set(Field.SUM.getIdx(), 1+row.get(Field.SUM.getIdx()));

            //Update SMUT summary on row i.
            smut.set(i, row);
            
        }catch(IllegalArgumentException e){
            //Display a prompt for the user.
            String failMsg = 
                    "Error: Unexpected character at position " + i+1 +
                    " encountered in comparison of sequences:\n" + pprof.getID() +
                    " and " + sprof.getID() + "\n\n" +
                    "SMUT summary can be computed only for aligned nucletide " +
                    "sequences with no frameshifting indels or ambiguities.";
            DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(failMsg));
            
            throw e;
        }
    }

    private void countDIFFS() {
    /*
     * Add counts of differences at each position to the SMUT summary.
     */
        AbstractTableModel atm = td.tableModel();
        int n = atm.getRowCount();

        //For each position in sequence.
        for (int i = 1; i < atm.getColumnCount(); i++) {

            //Count occurances of bases at that position.
            Map<String, Integer> map = new HashMap<String, Integer>();
            for (int j = 0; j < atm.getRowCount(); j++) {

                String value = (String) atm.getValueAt(j, i);
                int count = 1;
                if (map.containsKey(value))
                    count += map.get(value);
                map.put(value, count);
            }

            //Calculate number of pairwise differences at position i-1.
            int diffs = 0;
            for (int count: map.values())
                diffs += (n - count)*count;

            //Update DIFFS in row i-1 of SMUT summary.
            ArrayList<Integer> row = smut.get(i-1);
            row.set(Field.DIFFS.getIdx(), diffs/2);
            smut.set(i-1, row);
        }
    }

    private StringBuilder SMUT() {
    /*
     * Build SMUT summary output string.
     */
        //Prepare SMUT stream.
        StringBuilder smut_sb = new StringBuilder("");

        //Only if no problems where encountered and it is valid.
        if (smut != null) {
            //Update SMUT.
            //Count number of pairwise differences at each position in the sequences.
            countDIFFS();

            //Set SMUT header.
            smut_sb.append("POSITION,DIFFS,AC,AT,AG,CA,CT,CG,TA,TC,TG,GA,GC,GT,SUM,TRANSITION,TRANSVERSION,SILENT,NON-SILENT,EQUIVALENT,NON-EQUIV\n");

            //Build summary's rows.
            for (int i = 0; i < root.getSelf().profileLength(); i++){
                smut_sb.append(i+1);
                for (int j = 0; j < 20; j++){
                    smut_sb.append(",").append(smut.get(i).get(j));
                }
                smut_sb.append("\n");
            }
        }
        return smut_sb;
    }

    public TypingData<? extends Profile> getTypingData() {
        return td;
    }
}
