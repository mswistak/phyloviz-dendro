package net.phyloviz.dendro.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import net.phyloviz.algo.AbstractDistance;
import net.phyloviz.algo.Edge;
import net.phyloviz.goeburst.tree.GOeBurstNode;


public class TreeNode{
    
    private GOeBurstNode parent;
    private GOeBurstNode self;
    private ArrayList<TreeNode> children;
    private AbstractDistance<GOeBurstNode> dist;
    
    public TreeNode(GOeBurstNode s, Map<GOeBurstNode, ? extends Set<Edge<GOeBurstNode>>> mapping, AbstractDistance<GOeBurstNode> ad){
    /*
     * TreeNode constructor for root node.
     */
        parent = null;
        self = s;
        dist = ad;
        addChildren(mapping);
    }
    
    private TreeNode(GOeBurstNode p, GOeBurstNode s, Map<GOeBurstNode, ? extends Set<Edge<GOeBurstNode>>> mapping, AbstractDistance<GOeBurstNode> ad){
    /*
     * TreeNode constructor for non-root nodes.
     */
        this.parent = p;
        this.self = s;
        dist = ad;
        addChildren(mapping);
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof TreeNode){
            TreeNode tn = (TreeNode) o;
            return getSelf().equals(tn.getSelf());
        }
        return false;
    }
    
    private void addChildren(Map<GOeBurstNode, ? extends Set<Edge<GOeBurstNode>>> mapping) {
    /*
     * Add children of a TreeNode according to specified mapping.
     */
        children = new ArrayList<TreeNode>();
        
        //Get Set of Edges to the neighbours of self.
        Set<Edge<GOeBurstNode>> neigh = mapping.get(self);
        
        //Iterate over Egdes in the Set.
        for(Edge<GOeBurstNode> e: neigh){
            
            //Get both ends of the Edge.
            GOeBurstNode u = e.getU();
            GOeBurstNode v = e.getV();
            
            //For non-root TreeNodes continue to next Edge if one of the ends is the parent.
            if(parent != null){
                if(u.equals(parent) || v.equals(parent)){
                    continue;
                }
            }
            
            //Add node (not self!) to the list of children.
            if(u.equals(self)){
                children.add(new TreeNode(self, v, mapping, dist));
            }else{
                children.add(new TreeNode(self, u, mapping, dist));
            }
        }
    }

    public StringBuilder Newick() {
    /*
     * Recursively build Newick-formated tree output.
     */
        StringBuilder out = new StringBuilder("");
        
        //For non-leaves TreeNodes.
        if(!children.isEmpty()){
            
            //List of children is enclosed in parenthesis.
            out.append("(");
            
            //Iterate over TreeNode's children.
            Iterator<TreeNode> chi = children.iterator();
            
            //Recursively build output from each child.
            TreeNode ch = chi.next();
            out.append(ch.Newick());
            
            while(chi.hasNext()){
                ch = chi.next();
                out.append(",\n").append(ch.Newick());
            }
            
            out.append(")\n");
        }
        
        //Output self.
        out.append(self.getID());
        
        //For root TreeNode end with a ";".
        if(parent == null){
            out.append(";");
            
        //For non-root TreeNode output distance to parent.
        }else{
            out.append(":").append(getEdgeLevel());
        }
        return out;
    }
    
    public TreeNode improve() {
    /*
     * Set root TreeNode to minimize the height of the tree.
     */
        //Invokable for the root TreeNode only.
        assert(parent == null);
        
        //For one- and two-tier trees there is nothing to do.
        if(children.isEmpty() || !isMultiTier(1)){
            return this;
            
        //For multi-tier trees with root TreeNode having exactly one child
        //set the child as the new root TreeNode.
        }else if(children.size() == 1){
            return setNewRoot(0);
            
        //For multi-tier trees with root TreeNode having multiple children.
        }else{
            ArrayList<Integer> childrenHeights = new ArrayList<Integer>(children.size());
            ArrayList<Integer> subTreeHeights = new ArrayList<Integer>(children.size());
            
            int childHeight;
            int subTreeHeight;
            
            int maxi = -1;   //index of child rooting a subtree with maximum height
            int smaxi = -1;  //index of child rooting a subtree with second maximum height
            
            //Iterate over children.
            int i = 0;
            for(TreeNode child: children){
                
                //Get distance between root TreeNode and its child.
                //Calculate height of subtree rooted in child.
                childHeight = child.getEdgeLevel();
                subTreeHeight = child.getSubTreeHeight();
                
                //Save calculated metrics.
                childrenHeights.add(childHeight);
                subTreeHeights.add(subTreeHeight);
                
                //We consider the first child.
                if(maxi < 0){
                    maxi = i;
                    
                //If current height is greater than current maximum,
                //update current maximum and second maximum indices.
                }else if(subTreeHeight >= subTreeHeights.get(maxi)){
                    smaxi = maxi;
                    maxi = i;
                    
                //We consider the second child which deosn't root larger subtree
                //than the first child.
                }else if(smaxi < 0){
                    smaxi = i;
                    
                //If current height is greater than current second maximum,
                //update second maximum index.
                }else if(subTreeHeight >= subTreeHeights.get(smaxi)){
                    smaxi = i;
                }
                
                i++;
            }
            
            //Set the maxi child as the new root TreeNode if tree height
            //decreases after transformation.
            if(subTreeHeights.get(maxi) >
               subTreeHeights.get(smaxi) + childrenHeights.get(maxi))
                return setNewRoot(maxi);
            
            //No improvement can be done.
            return this;
        }
    }
    
    private int getSubTreeHeight() {
    /*
     * Calculate the height of subtree rooted in TreeNode.
     */
        int maxHeight = 0;
        int tmp;
        
        //Select maximum heights of subtrees rooted in children.
        for(TreeNode child: children){
            tmp = child.getSubTreeHeight();
            if(maxHeight < tmp){
                maxHeight = tmp;
            }
        }
        
        //Add the distance to parent.
        return maxHeight + getEdgeLevel();
    }

    private int getEdgeLevel() {
    /*
     * Calculate distance to parent according to specified metric.
     */
        return (parent == null) ? 0 : dist.level(self, parent);
    }

    private boolean isMultiTier(int tier){
    /*
     * Check if a subtree rooted in TreeNode has multiple (>2) tiers.
     */
        //Check if we already are beyond tier 2.
        if(tier > 2)
            return true;
        
        //Explore possible next tiers.
        for(TreeNode child: children)
            if(child.isMultiTier(tier+1))
                return true;
        
        return false;
    }

    private void setParent(GOeBurstNode newParent) {
    /*
     * Set new parent of the TreeNode.
     */
        parent = newParent;
    }

    private void addChild(TreeNode newChild) {
    /*
     * Add a child of the TreeNode
     */
        children.add(newChild);
    }

    private TreeNode setNewRoot(int i) {
    /*
     * Set i-th child of the root TreeNode as the new root.
     */
        //Invokable for the root TreeNode only.
        assert(parent == null);
        
        //Remove the new root TreeNode from list of children of the old root.
        TreeNode newRoot = children.remove(i);
        
        //Set the old root's parent to new root.
        setParent(newRoot.getSelf());
        
        //Set the new root's parent to null.
        newRoot.setParent(null);
        
        //Add the old root as a child of the new root.
        newRoot.addChild(this);
        
        return newRoot;
    }

    public GOeBurstNode getSelf() {
    /*
     * Return the GOeBurstNode associated with the TreeNode.
     */
        return self;
    }

    public Collection<TreeNode> getChildren() {
    /*
     * Return the collection of children of the TreeNode.
     */
        return children;
    }

    public GOeBurstNode getParent() {
    /*
     * Return the GOeBurstNode associated with the parent of the TreeNode.
     */
        return parent;
    }
    
}
