package net.phyloviz.dendro;

import net.phyloviz.dendro.ui.SaveNewickAction;
import org.openide.util.actions.NodeAction;
import org.openide.util.lookup.ServiceProvider;


@ServiceProvider(service = SaverProvider.class)
public class NewickSaverProvider extends SaverProvider {
    
    private static final String customName = "Newick";
    private NodeAction action;

    @Override
    public String toString() {
            return customName;
    }

    public boolean asynchronous() {
            return false;
    }

    @Override
    public NodeAction getAction() {
            if (action == null)
                    action = new SaveNewickAction();
            return action;
    }

}