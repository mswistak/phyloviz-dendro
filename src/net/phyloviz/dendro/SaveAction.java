package net.phyloviz.dendro;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import net.phyloviz.algo.AbstractDistance;
import net.phyloviz.algo.Edge;
import net.phyloviz.core.data.Profile;
import net.phyloviz.core.data.TypingData;
import net.phyloviz.dendro.tree.Tree;
import net.phyloviz.goeburst.tree.GOeBurstNode;
import net.phyloviz.goeburst.tree.GOeBurstMSTResult;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;


public abstract class SaveAction extends NodeAction {
/*
 * Manages saving trees and summaries.
 */
    
    @Override
    protected void performAction(Node[] nodes){

        //Get node's TypingData.
        TypingData<? extends Profile> td = (TypingData<? extends Profile>) nodes[0].getParentNode().getLookup().lookup(TypingData.class);
        
        //Get MST result.
        GOeBurstMSTResult gr = nodes[0].getLookup().lookup(GOeBurstMSTResult.class);
        AbstractDistance<GOeBurstNode> ad = gr.getDistance();
        Collection<Edge<GOeBurstNode>> edges = gr.getEdges();

        TreeSaver ts = createTreeSaver(td, ad, edges);
        
        try{
            ts.save();
            
            String confMsg = "Successfully saved the tree.";
            DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(confMsg));
        } catch (IOException ioe) {
            String failMsg = "Error: " + ioe.getMessage();
            DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(failMsg));
        }
    }
    
    @Override
    protected boolean enable(Node[] nodes) {
        return nodes.length == 1;
    }

    @Override
    public String getName() {
        return "";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return null;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }

    //Implementations create appropriate TreeSaver objects.
    protected abstract TreeSaver createTreeSaver(TypingData<? extends Profile> td, AbstractDistance<GOeBurstNode> ad, Collection<Edge<GOeBurstNode>> edges);
    
    
    protected abstract class TreeSaver implements SaveCookie {
    /*
     * Private class handling actual saving.
     */
        private Tree tree;
        private boolean smut;
        private String ext;

        public TreeSaver(Tree tree, boolean smut, String ext){
            this.tree = tree;
            this.smut = smut;
            this.ext = ext;
        }

        @Override
        public void save() throws IOException {
            
            //Setup the saving dialog and get output base name.
            String title = "Saving tree...";
            File f = new FileChooserBuilder(SaveAction.class).setTitle(title).showSaveDialog();
            
            //Sanity checks.
            if (f != null) {
                try {
                    
                    //Create File object for Newick output and save tree.
                    File f_tree = new File(f.getAbsolutePath() + ext);
                    if(!checkFile(f_tree))
                        return;
                    saveTree(f_tree.getAbsoluteFile(), tree);
                    
                    //Create File object for MUT output.
                    File f_mut = new File(f.getAbsolutePath() + "_mut.csv");
                    if(!checkFile(f_mut))
                        return;
                    
                    //Check if SMUT flag is active.
                    if(smut){
                        
                        //Create File object for SMUT output.
                        File f_smut = new File(f.getAbsolutePath() + "_smut.csv");
                        if(!checkFile(f_smut))
                            return;
                        
                        //Create and save MUT and SMUT summaries.
                        saveMUT(f_mut.getAbsoluteFile(), f_smut.getAbsoluteFile(), tree);
                    } else {
                        
                        //Create and save only MUT summary.
                        saveMUT(f_mut.getAbsoluteFile(), tree);
                    }

                } catch (IOException ioe) {
                    String failMsg = "Error: " + ioe.getMessage();
                    DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(failMsg));
                }
            } else {
                throw new FileNotFoundException("Unknown file!");
            }
        }

        //Saves computed tree in an appropriate file format.
        abstract protected void saveTree(File f, Tree tree) throws FileNotFoundException;

        private void saveMUT(File f_mut, Tree tree) throws FileNotFoundException {
        /*
         * Saves MUT summary for the tree.
         */
            PrintWriter pw_mut = new PrintWriter(f_mut);
            
            //false denotes that there will be no SMUT summary.
            StringBuilder[] mut = tree.MUT(false);
            pw_mut.print(mut[0]);
            pw_mut.close();
        }

        private void saveMUT(File f_mut, File f_smut, Tree tree) throws FileNotFoundException {
        /*
         * Saves MUT and SMUT summaries for the tree.
         */
            //true denotes that both summaries will be created.
            StringBuilder[] mut = tree.MUT(true);
            
            PrintWriter pw_mut = new PrintWriter(f_mut);
            pw_mut.print(mut[0]);
            pw_mut.close();
            
            //Delete SMUT file if SMUT summary was empty.
            if ( mut[1].equals(new StringBuilder("")) ) {
                f_smut.delete();
            } else {
                PrintWriter pw_smut = new PrintWriter(f_smut);
                pw_smut.print(mut[1]);
                pw_smut.close();
            }
        }

        private boolean checkFile(File f) throws IOException {
        /*
         * Sanity checks for files.
         * Handling unexpected problems with files.
         */
            if (!f.exists()) {
                if (!f.createNewFile()) {
                    String failMsg = "Unable to create " + f.getName();
                    DialogDisplayer.getDefault().notify(new NotifyDescriptor.Message(failMsg));
                    return false;
                }
            } else {
                String overwriteMessage = "Overwriting " + f.getName();
                Object userChose = DialogDisplayer.getDefault().notify(new NotifyDescriptor.Confirmation(overwriteMessage));
                if (NotifyDescriptor.CANCEL_OPTION.equals(userChose)) {
                    return false;
                }
            }
            return true;
        }
    }
}
