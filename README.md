#Overview#

PHYLOViZ Dendro is a plug-in for PHYLOViZ software (http://www.phyloviz.net/wiki/) which provides saving MSTs (Minimum Spanning Trees) in standard text formats (Newick, NeXML, Dendro) and creating a summary listing all mutations along the edges of the tree (MUT summary). Additionally for Aligned Sequences and Aligned Sequences (FASTA) another summary is created. It describes position specific mutations in the dataset (SMUT summary). MUT and SMUT are comma-separated files.

In MUT each row corresponds to one mutation between nodes of the tree. MUT has 4 columns:

1. **PARENT** – id of parent node;
2. **CHILD** – id of child node;
3. **POSITION** – position of mutation;
4. **MUTATION** – mutation type (e.g. A=>T or 3=>4 depending on data type).

In SMUT each row gives information about mutations in one specific position in the sequence. SMUT has 21 columns:

1. **POSITION** - position in sequence;
2. **DIFFS** – number of pairwise differences across the dataset;
3. **AC** – number of A to C substitutions between all connected nodes of the tree;
4. **AT** – number of A to T substitutions between all connected nodes of the tree;
5. **AG** – number of A to G substitutions between all connected nodes of the tree;
6. **CA** – number of C to A substitutions between all connected nodes of the tree;
7. **CT** – number of C to T substitutions between all connected nodes of the tree;
8. **CG** – number of C to G substitutions between all connected nodes of the tree;
9. **TA** – number of T to A substitutions between all connected nodes of the tree;
10. **TC** – number of T to C substitutions between all connected nodes of the tree;
11. **TG** – number of T to G substitutions between all connected nodes of the tree;
12. **GA** – number of G to A substitutions between all connected nodes of the tree;
13. **GC** – number of G to C substitutions between all connected nodes of the tree;
14. **GT** – number of G to T substitutions between all connected nodes of the tree;
15. **SUM** – total number of mutations between all connected nodes of the tree;
16. **TRANSITION** – total number of transitions (i.e. A <=> G and C <=> T substitutions) between all connected nodes of the tree;
17. **TRANSVERSION** – total number of transversions between all connected nodes of the tree;
18. **SILENT** – number of silent mutations
19. **NON-SILENT** – number of missense mutations
20. **EQUIVALENT** – number of mutations resulting in substitution of amino acid to any with similar molecular properties (e.g. Val <=> Ala or Glu <=> Asp)
21. **NON-EQUIV** – number of mutations resulting in substitution of amino acid to any with different molecular properties

#Installation

To install PHYLOViZ download and use installer specific for your operating system and let the installation wizard guide you through the process (see installers branch). Windows installer is valid up to Win7. For Windows 8 please use universal zip package instead. Note that on any operating system you first must have a JRE environment installed.

As has already been mentioned on main project's website you may also need to adjust some parameters in etc/phyloviz.conf with respect to memory usage (especially on 32-bit Windows). Specifically set -J-Xmx parameter (MaxHeapSize) in the configuration file to maximum allowed by your java.

To include Dendro plug-in to your distribution of PHYLOViZ, if you have the program already installed, please check if it is available for download from within PHYLOViZ. In order to do this run PHYLOViZ and go to Tools >> Plugins.

![Tools >> Plugins](http://students.mimuw.edu.pl/~ms318996/phyloviz/install_plugin_1.png)

Firstly check if Dendro module is on the list in Available Plugins bookmark. If it is, tick the modules you want to install and click Install button in bottom-left corner of the window.

![Available Plugins](http://students.mimuw.edu.pl/~ms318996/phyloviz/install_plugin_2.png)

Otherwise go to Downloaded bookmark, click Add Plugins button in upper-left corner of the window and browse for nbm file which can be downloaded from branch nbm.

![Downloaded](http://students.mimuw.edu.pl/~ms318996/phyloviz/install_plugin_3.png)

Tick modules you want to install, click Install button in the bottom-left corner of the window and go through the wizard to install the module.

![Install](http://students.mimuw.edu.pl/~ms318996/phyloviz/install_plugin_4.png)

#Manual

Basic tutorial for PHYLOViZ software is available at project's website (http://www.phyloviz.net/wiki/tutorial/).

The usage of PHYLOViZ Dendro module is very intuitive and pretty straightforward. Once you have it included into your distribution of PHYLOViZ, you basically need:

* Load your Dataset.

![File >> Load](http://students.mimuw.edu.pl/~ms318996/phyloviz/load_dataset_1.png)

* Choose data type from the dropdown list.

![Filetype](http://students.mimuw.edu.pl/~ms318996/phyloviz/load_dataset_2.png)

* Browse your input file.

![Browse data](http://students.mimuw.edu.pl/~ms318996/phyloviz/load_dataset_3.png)

* Browse your additional isolate data if you have any.

![Browse isolate data](http://students.mimuw.edu.pl/~ms318996/phyloviz/load_dataset_4.png)

* Compute full MST (NOTE: saving trees in text formats is available only for full MSTs).

![Compute MST](http://students.mimuw.edu.pl/~ms318996/phyloviz/compute_tree_1.png)

* Choose distance measure.

![Distance](http://students.mimuw.edu.pl/~ms318996/phyloviz/compute_tree_2.png)

* Save computed tree in text format of your choice (from the list) along with MUT summary.

![Save tree](http://students.mimuw.edu.pl/~ms318996/phyloviz/save_tree_1.png)

* For sequence data you will be asked if you want an additional SMUT summary computed.

![SMUT summary](http://students.mimuw.edu.pl/~ms318996/phyloviz/save_tree_2.png)

* You have successfully saved your tree.

![Tree saved](http://students.mimuw.edu.pl/~ms318996/phyloviz/save_tree_3.png)